angular.module('grimReader',[]).
    controller('GrimController',['$scope','$sce', function($scope, $sce){
        $scope.data = null;
        $scope.theme = null;
        $scope.page = null;
        $scope.card = null;

        $scope.processData = function(data){
            var processedData = data.map(function (theme) {
                theme.trustedThemeName = theme.trustedThemeName || $sce.trustAsHtml(theme.themeName);
                theme.pages = theme.pages.map(function (page) {
                    page.trustedPageName = page.trustedPageName || $sce.trustAsHtml(page.pageName);
                    page.cards = page.cards.map(function (card) {
                        card.trustedCardName = card.trustedCardName || $sce.trustAsHtml(card.cardName);
                        card.trustedCardIntro = card.trustedCardIntro || $sce.trustAsHtml(card.cardIntro);
                        card.trustedCardDescription = card.trustedCardDescription || $sce.trustAsHtml(card.cardDescription);
                        card.trustedCardPathName = card.trustedCardPathName || $sce.trustAsHtml(theme.themeName + " &gt; " + page.pageName + " &gt; " + card.cardName);
                        return card;
                    });
                    return page;
                });
                return theme;
            });

            var lastCard = $scope.restoreReadCardsStatus(processedData);

            return {data:processedData, bookmark: lastCard};
        };

        $scope.setData = function(data){
            var recoveredData = $scope.processData(data)
            $scope.data = recoveredData.data;
            $scope.setTheme(recoveredData.bookmark.theme);
            $scope.setPage(recoveredData.bookmark.page);
            $scope.setCard(recoveredData.bookmark.card);
        };

        $scope.setTheme = function(themeName){
            $scope.theme = $scope.data.reduce(function(acc, theme){
                return (theme.themeName === themeName) ? theme : acc;
            });

            $scope.setPage($scope.theme.pages[0].pageName);
        };

        $scope.setPage = function(pageName){
            $scope.page = $scope.theme.pages.reduce(function(acc, page){
                return (page.pageName === pageName) ? page : acc;
            });

            $scope.setCard($scope.page.cards[0].cardId);
        };

        $scope.setCard = function(cardId){
            $scope.card = $scope.page.cards.reduce(function(acc, card){
                return (card.cardId === cardId) ? card : acc;
            });

            $scope.consolidateReadCardStatus({theme: $scope.theme.themeName, page: $scope.page.pageName, card: cardId});
        };

        $scope.updateThemeStatus = function(theme){
            theme.isRead = theme.pages.reduce(function (accPage, page) {
                return (page.isRead || page.isComplete) ? true : accPage;
            }, false);

            theme.isComplete = theme.pages.reduce(function (accPage, page) {
                return (page.isComplete ? accPage : false);
            }, true);
        };

        $scope.updatePageStatus = function(page){
            page.isRead = page.cards.reduce(function (accCard, card) {
                return (card.isRead) ? true : accCard;
            }, false);

            page.isComplete = page.cards.reduce(function (accCard, card) {
                return (card.isRead ? accCard : false);
            }, true);
        };

        $scope.nextCard = function(current){
            $scope.card.isRead = true;
            $scope.updatePageStatus($scope.page);
            $scope.updateThemeStatus($scope.theme);

            var nextCard = $scope.page.cards.reduce(function(previous, current){
                return (current === $scope.card || previous === $scope.card) ? current : previous;
            });

            if(nextCard === $scope.card){
                var nextPage = $scope.theme.pages.reduce(function(previous, current){
                    return (current === $scope.page || previous === $scope.page) ? current : previous;
                });

                if(nextPage === $scope.page){
                    var nextTheme = $scope.data.reduce(function(previous, current){
                        return (current === $scope.theme || previous === $scope.theme) ? current : previous;
                    });

                    if(nextTheme === $scope.theme){
                        $scope.setTheme($scope.data[0].themeName);
                    }else{
                        $scope.setTheme(nextTheme.themeName);
                    }
                }else{
                    $scope.setPage(nextPage.pageName);
                }
            }else{
                $scope.setCard(nextCard.cardId);
            }
        };

        $scope.prevCard = function(current){
            var prevCard = $scope.page.cards.reduceRight(function (next, current) {
                return (current === $scope.card || next === $scope.card) ? current : next;
            });

            if (prevCard === $scope.card) {
                var prevPage = $scope.theme.pages.reduceRight(function (next, current) {
                    return (current === $scope.page || next === $scope.page) ? current : next;
                });

                if (prevPage === $scope.page) {
                    var prevTheme = $scope.data.reduceRight(function (next, current) {
                        return (current === $scope.theme || next === $scope.theme) ? current : next;
                    });

                    if (prevTheme === $scope.theme) {
                        $scope.setTheme($scope.data[$scope.data.length - 1].themeName);
                    } else {
                        $scope.setTheme(prevTheme.themeName);
                    }

                    $scope.setPage($scope.theme.pages[$scope.theme.pages.length - 1].pageName);
                } else {
                    $scope.setPage(prevPage.pageName);
                }

                $scope.setCard($scope.page.cards[$scope.page.cards.length - 1].cardId);
            } else {
                $scope.setCard(prevCard.cardId);
            }
        };

        $scope.resolveResourceUrl = function (path) {
            return "http://www.bungie.net" + ((path) ? path : '/common/destiny_content/grimoire/hr_images/Places-psprites-sm_202a9b01dc0cebada2cc3dabe4df306f.jpg');
        };

        $scope.getSelectedThemeClass = function (themeName) {
            var classes;
            $scope.updateThemeStatus($scope.theme);

            classes = ($scope.theme.themeName === themeName) ? "selectedTheme" : "";
            classes += $scope.data.filter(function(theme){
                return (theme.themeName === themeName);
            }).reduce(function(acc, selTheme){
                return (selTheme.isComplete) ? " completeTheme" : (selTheme.isRead) ? " readTheme" : acc;
            }," unreadTheme");

            return classes
        };

        $scope.getSelectedPageClass = function (pageName) {
            var classes;
            $scope.updatePageStatus($scope.page);

            classes = ($scope.page.pageName === pageName) ? "selectedPage" : "";
            classes += $scope.theme.pages.filter(function(page){
                return (page.pageName === pageName);
            }).reduce(function(acc, selPage){
                return (selPage.isComplete) ? " completePage" : (selPage.isRead) ? " readPage" : acc;
            }," unreadPage");

            return classes;
        };

        $scope.getSelectedCardClass = function(cardId){
            var classes;

            classes = ($scope.card.cardId === cardId) ? "selectedCard" : "";
            classes += $scope.page.cards.filter(function(card){
                return (card.cardId === cardId);
            }).reduce(function(acc,selCard){
                return (selCard.isRead) ? " readCard" : acc;
            }," unreadCard");

            return classes;
        };

        $scope.consolidateReadCardStatus = function(lastCard){
            var readCardsStatus = $scope.data.reduce(function(accTheme,theme) {
                return theme.pages.reduce(function (accPage, page) {
                    return page.cards.reduce(function (accCard, card) {
                        accCard[card.cardId] = !!card.isRead;
                        return accCard;
                    }, accPage)
                }, accTheme);
            },{});

            localStorage.readCards = JSON.stringify(readCardsStatus);
            localStorage.lastCard = JSON.stringify(lastCard);
            return readCardsStatus;
        };

        $scope.restoreReadCardsStatus = function(data){
            var readCardsStatus =  (localStorage.readCards) ? JSON.parse(localStorage.readCards) : {};
            data.forEach(function(theme){
                theme.pages.forEach(function(page){
                    page.cards.forEach(function(card){
                        card.isRead = readCardsStatus[card.cardId] || false;
                    });
                    $scope.updatePageStatus(page);
                });
                $scope.updateThemeStatus(theme);
            });

            var lastCard = (localStorage.lastCard) ? JSON.parse(localStorage.lastCard) : {theme:data[0].themeName, page:data[0].pages[0].pageName, card:data[0].pages[0].cards[0].cardId};
            return lastCard;
        };

        $scope.init = function(){
            var ref = new Firebase('https://blistering-torch-3502.firebaseio.com/');

            ref.on("value", function (snapshot) {
                $scope.$apply(
                    $scope.setData(snapshot.val())
                );
            }, function (errorObject) {
                alert('Could not load data from firebase. Try again later.');
            });
        }

    }]);
