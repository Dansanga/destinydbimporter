/**
 * Created by dsobrado on 12/22/2014.
 */

"use strict";

var app = function(global){
    var targetDomain = "http://www.bungie.net/",
        ref = null;


    function createCard(id, name, intro, desc, thumbData,  image){

        var newElement = document.getElementsByClassName('template')[0].cloneNode(10);

        newElement.getElementsByClassName('title')[0].innerHTML = name;
        newElement.getElementsByClassName('id')[0].innerHTML = id;

        newElement.getElementsByClassName('thumbnail')[0].style.height = thumbData.details.height+"px";
        newElement.getElementsByClassName('thumbnail')[0].style.width = thumbData.details.width+"px";
        newElement.getElementsByClassName('thumbnail')[0].style.backgroundImage = "url("+targetDomain+thumbData.image+")";
        newElement.getElementsByClassName('thumbnail')[0].style.backgroundPositionX = (thumbData.details.x * -1)+"px";
        newElement.getElementsByClassName('thumbnail')[0].style.backgroundPositionY = (thumbData.details.y * -1)+"px";

        newElement.getElementsByClassName('content')[0].id = id;
        newElement.getElementsByClassName('art')[0].src = targetDomain+image;
        newElement.getElementsByClassName('name')[0].innerHTML = name;
        newElement.getElementsByClassName('intro')[0].innerHTML = intro || "";
        newElement.getElementsByClassName('desc')[0].innerHTML = desc;
        newElement.getElementsByClassName('ident')[0].innerHTML = id;

        return newElement;
    };

    function getZipFile(zipLocation){

        //set location of z-workers
        zip.workerScripts = {
            deflater: ['/Test/lib/zip/z-worker.js', '/Test/lib/zip/deflate.js'],
            inflater: ['/Test/lib/zip/z-worker.js', '/Test/lib/zip/inflate.js']
        };

        // use a BlobReader to read the zip from a Blob object
        zip.createReader(new zip.Data64URIReader(zipLocation), function (reader) {

            // get all entries from the zip
            reader.getEntries(function (entries) {
                if (entries.length) {

                    // get first entry content as text
                    entries[0].getData(new zip.TextWriter(), function (text) {
                        // text contains the entry data as a String
                        console.log(text);

                        // close the zip reader
                        reader.close(function () {
                            // onclose callback
                        });

                    }, function (current, total) {
                        // onprogress callback
                    });
                }
            });
        }, function (error) {
            // onerror callback
        });
    };

    function loadFromSqlDb(){
       var xhr = new XMLHttpRequest(),
           grimCards = null,
           grimData = null;

        xhr.open('GET', '/Test/data/destiny.content.v2.sqlite', true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function (e) {
            var uInt8Array = new Uint8Array(this.response),
                db = new SQL.Database(uInt8Array),
                contents = db.exec("SELECT * FROM DestinyGrimoireCardDefinition");

            // contents is now [{columns:['col1','col2',...], values:[[first row], [second row], ...]}]
            grimCards = parseCards(contents[0].values);
            contents = db.exec("SELECT * FROM DestinyGrimoireDefinition");
            app.data = parseCategories(contents[0].values[0][1], grimCards);
            displayCards(app.data);
            app.ref.set(app.data);
        };

        xhr.send();


    }

    function parseCategories(categoryCollection, cards){
        var themeCollObj = JSON.parse(categoryCollection).themeCollection,
            categories = [];

        categories = themeCollObj.map(function(theme){
            return {
                themeName: theme.themeName || "",
                icon: theme.highResolution.smallImage || "",
                image: theme.highResolution.image || "",
                pages: theme.pageCollection.map(function (page) {
                    return {
                        pageName: page.pageName || "",
                        icon: page.highResolution.smallImage || "",
                        image: page.highResolution.image || "",
                        cards: page.cardBriefs.map(function (cardBrief) {
                            return cards[cardBrief.cardId];
                        })
                    };
                })
            };
        });

        return categories;
    }

    function parseCards(cards){
        var cardsRes = cards.reduce(function(result, card) {
                var cardObj = JSON.parse(card[1]);

                result[cardObj.cardId] = {
                    cardId: cardObj.cardId || "",
                    cardName: cardObj.cardName || "",
                    cardIntro: cardObj.cardIntro || "",
                    cardDescription: cardObj.cardDescription || "",
                    icon: cardObj.highResolution.smallImage || "",
                    image: cardObj.highResolution.image || ""
                };

                return result;
            },{});

        return cardsRes;
    };

    function displayCards(data){
        var htmlString = "";

        data.forEach(function(theme){
            theme.pages.forEach(function(page){
                page.cards.forEach(function(cardObj){
                    var cardNode = createCard(cardObj.cardId,
                        cardObj.cardName,
                        cardObj.cardIntro,
                        cardObj.cardDescription,
                        {
                            image: cardObj.icon.sheetPath,
                            details: cardObj.icon.rect
                        },
                        cardObj.image.sheetPath
                    );
                    htmlString += cardNode.innerHTML;
                });
            });
        });

        document.writeln(htmlString);
    }

    return  {
        data: null,
        ref: null,
        init: function(){

            //var xhr2 = new XMLHttpRequest();
            //xhr2.open('GET', 'http://www.bungie.net/common/destiny_content/sqlite/en/world_sql_content_41764675ffa70c7fca0a6873b77aabeb.content', true);
            ////xhr2.open('GET', '/Test/data/world_sql_content_41764675ffa70c7fca0a6873b77aabeb.content.zip', true);
            //
            //xhr2.responseType = 'blob';
            //xhr2.onload = function (e) {
            //    debugger
            //    var pt = this.response;
            //};
            //xhr2.onerror = function(x){
            //    debugger
            //    console.log(x);
            //}
            //xhr2.send();

            this.ref = new Firebase('https://blistering-torch-3502.firebaseio.com/');

            this.ref.on("value", function (snapshot) {
                if(!snapshot.val()){
                    loadFromSqlDb();
                    prompt('There is no data in firebase. Do you want to download the source data for import?');
                }else {
                    displayCards(snapshot.val());
                }
            }, function (errorObject) {
                loadFromSqlDb();
                alert('Could not load data from firebase. Try again later.');
            });


        },

        showHide: function(caller){
            var id = caller.getElementsByClassName('id')[0].innerHTML,
                allCards = document.getElementsByClassName('content');

            Array.prototype.forEach.call(allCards,function(element){
                if(element.id === id) {
                    element.style.display = element.style.display === 'none' ? 'block' : 'none';
                }else{
                    element.style.display = 'none';
                }
            });
        }
    };
}(this);
